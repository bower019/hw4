﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace HW4V2.Models
{
	public class Game : ContentPage
	{
        public string Name
        {
            get;
            set;
        }

        public string Genre
        {
            get;
            set;
        }

        public string details
        {
            get;
            set;
        }


        public string Image
        {
            get;
            set;
        }

    }
}
