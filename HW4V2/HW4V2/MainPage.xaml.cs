﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using HW4V2.Models;

namespace HW4V2
{
    public partial class MainPage : ContentPage
    {
        ObservableCollection<Game> value = new ObservableCollection<Game>();
        public MainPage()
        {
            InitializeComponent();

            PopulateList();
        }

        

        private void PopulateList()
        {
            
            
                var Game1 = new Game();
                Game1.Name = "Halo";
                Game1.Genre = "Science Fiction";
                Game1.details = "Xbox";
            Game1.Image = "halo.png";
                value.Add(Game1);

            var Game2 = new Game();
            Game2.Name = "Stardew Valley";
            Game2.Genre = "Simulator";
            Game2.details = "Multiplatform";
            Game2.Image = "stardew.png";
            value.Add(Game2);

            var Game3 = new Game();
            Game3.Name = "Armello";
            Game3.Genre = "Strategy";
            Game3.details = "PC";
            Game3.Image = "armello.png";
            value.Add(Game3);

            var Game4 = new Game();
            Game4.Name = "Fallout";
            Game4.Genre = "Post Apocalypse";
            Game4.details = "Multiplatform";
            Game4.Image = "fallout.png";
            value.Add(Game4);

            var Game5 = new Game();
            Game5.Name = "No Man's Sky";
            Game5.Genre = "Science Fiction";
            Game5.details = "Multiplatform";
            Game5.Image = "nms.png";
            value.Add(Game5);
            List.ItemsSource = value;
            
        
        }

        private void List_Refreshing(object sender, EventArgs e)
        {
            PopulateList();

            List.IsRefreshing = false;

        }

        public void Menu_Clicked(object sender, EventArgs e)
        {
            

        }







    }

}